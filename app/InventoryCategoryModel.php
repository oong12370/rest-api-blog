<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryCategoryModel extends Model
{
    protected $table = 'tb_inventory_category';

    protected $fillable = [
    	'nama_kategori',
    	'deskripsi',
    	'created_at',
    	'updated_at'
    ];

    //relasi one to many (Saya memiliki banyak anggota di model .....)
    public function get_inventory(){
    	return $this->hasMany('App\\InventoryModel', 'kategori', 'id');
    }
}
