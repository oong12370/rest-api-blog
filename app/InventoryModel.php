<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InventoryModel extends Model
{
	protected $table = 'tb_inventory';

    protected $primaryKey = 'id';

    protected $fillable = [
    	'nama_barang',
    	'jumlah',
    	'kategori',
    	'deskripsi',
    	'created_at',
    	'updated_at'
    ];

    //relasi many to one (Saya adalah anggota dari model ......)
    public function get_kategori(){
        return $this->belongsTo('App\\InventoryCategoryModel', 'kategori', 'id');
    }

}
