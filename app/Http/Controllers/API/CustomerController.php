<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\CustomerModel;
use App\Model\ServiceModel;
use Validator;
use App\Http\Resources\Customer as CustomerResource;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = CustomerModel::all();
        return CustomerResource::collection($customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $customer = CustomerModel::create($input);
   
        return response()->json(
             200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = CustomerModel::find($id);
        return $customers;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name =$request->name;
        $telp =$request->telp;
        $alamat =$request->alamat;
        $input = $request->all();
        $customer = CustomerModel::find($id);
        $customer->name =$name;
        $customer->telp =$telp;
        $customer->alamat =$alamat;
        $customer->save();
        
        return response()->json(
            200
       );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = CustomerModel::find($id);
        $customer->delete();
        return response()->json(
            200
       );
    }
}
