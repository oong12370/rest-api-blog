<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Kategory;
use Validator;
use App\Http\Resources\Kategory as KategoryResource;

class KategoryController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kategory = Kategory::get();
        return KategoryResource::collection($kategory);
        // $kategory = Kategory::all();
    
        return $this->sendResponse(KategoryResource::collection($kategory), 'Kategory retrieved successfully.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
            'ket' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $kategory = Kategory::create($input);
   
        return $this->sendResponse(new KategoryResource($kategory), 'Kategory created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kategory = Kategory::find($id);
  
        if (is_null($kategory)) {
            return $this->sendError('Kategory not found.');
        }
   
        return $this->sendResponse(new KategoryResource($kategory), 'Kategory retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kategory $kategory)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'name' => 'required',
            'ket' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $Kategory->name = $input['name'];
        $Kategory->ket = $input['detail'];
        $Kategory->save();
   
        return $this->sendResponse(new KategoryResource($kategory), 'Kategory updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kategory $kategory)
    {
        $kategory->delete();
   
        return $this->sendResponse([], 'kategory deleted successfully.');
    }
}
