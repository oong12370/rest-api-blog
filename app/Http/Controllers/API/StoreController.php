<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\StoreModel;
use App\Model\ServiceModel;
use App\Http\Resources\Store as StoreResource;
use Illuminate\Http\Response;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $store = StoreModel::all();
        return StoreResource::collection($store);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $store = StoreModel::create($input);
   
        return response()->json(
             200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = StoreModel::find($id);
        return $store;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kodeBest =$request->kodeBest;
        $nmStore =$request->nmStore;
        $alamat =$request->alamat;
        $store = StoreModel::find($id);
        $store->kodeBest =$kodeBest;
        $store->nmStore =$nmStore;
        $store->alamat =$alamat;
        $store->save();
        
        return response()->json(
            200
       );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $store = StoreModel::find($id);
        $store->delete();
        return response()->json(
            200
       );
    }
}
