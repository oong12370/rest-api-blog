<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategory extends Model
{
    protected $fillable = ['name'];

    public function images(){
        $this->hasMany('App\Images');
    }
    public $table = "kategory";
}
