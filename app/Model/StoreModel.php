<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StoreModel extends Model
{
    protected $table = 'm_store';

    protected $fillable = [
    	'kodeBest',
    	'nmStore',
        'alamat',
        'create_at',
    	'updated_at'
    ];

    //relasi one to many (Saya memiliki banyak anggota di model .....)
    public function get_service(){
    	return $this->hasMany('App\\Model\\ServiceModel', 'store', 'id');
    }
}
