<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServiceModel extends Model
{
    protected $table = 'tb_inventory';

    protected $primaryKey = 'id';

    protected $fillable = [
    	// 'nama_barang',
    	// 'jumlah',
    	// 'kategori',
    	// 'deskripsi',
    	// 'created_at',
    	// 'updated_at'
    ];

    //relasi many to one (Saya adalah anggota dari model ......)
    public function get_customer(){
        return $this->belongsTo('App\\Model\\CustomerModel', 'customer', 'id');
    }
}
