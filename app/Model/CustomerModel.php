<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CustomerModel extends Model
{
    protected $table = 'm_customer';

    protected $fillable = [
    	'name',
    	'telp',
        'alamat',
        'create_at',
    	'updated_at'
    ];

    //relasi one to many (Saya memiliki banyak anggota di model .....)
    public function get_service(){
    	return $this->hasMany('App\\Model\\ServiceModel', 'customer', 'id');
    }
}
