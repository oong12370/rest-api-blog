<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_inventory', function(Blueprint $kolom){
            $kolom->increments('id'); //sebagai primary key
            $kolom->string('nama_barang');
            $kolom->integer('jumlah');
            $kolom->integer('kategori');
            $kolom->text('deskripsi')->nullable();
            $kolom->timestamps(); // membuat kolom created_at & updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_inventory');
    }
}
