<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_service', function(Blueprint $kolom){
            $kolom->increments('id'); //sebagai primary key
            $kolom->date('tglTerima');
            $kolom->string('noSf');
            $kolom->string('pengiriman');
            $kolom->string('type');
            $kolom->date('tglGaransi');
            $kolom->string('kelengkapan');
            $kolom->string('kerusakan');
            $kolom->string('realisasi');
            $kolom->double('biaya');
            $kolom->date('tglSelesai');
            $kolom->date('tglAmbil');
            $kolom->date('tglBayar');
            $kolom->string('description');
            $kolom->string('sn');
            $kolom->integer('tekisi');
            $kolom->integer('store');
            $kolom->integer('customer');
            $kolom->timestamps(); // membuat kolom created_at & updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_service');
    }
}
