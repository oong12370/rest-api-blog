<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_inventory_category', function(Blueprint $kolom){
            $kolom->increments('id'); //primary key
            $kolom->string('nama_kategori');
            $kolom->text('deskripsi')->nullable(); //boleh kosong
            $kolom->timestamps(); //created_at + updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_inventory_category');
    }
}
