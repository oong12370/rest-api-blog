<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeknisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_teknisi', function(Blueprint $kolom){
            $kolom->increments('id'); //primary key
            $kolom->string('name');
            $kolom->mediumInteger('telp');
            $kolom->text('alamat'); //boleh kosong
            $kolom->timestamps(); //created_at + updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_teknisi');
    }
}
